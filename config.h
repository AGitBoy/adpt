#ifndef CONFIG_H
#define CONFIG_H

// Default string used when adapter is plugged in
#define ONSTR "Plugged in"

// Default string used when adapter isn't plugged in
#define OFFSTR "Unplugged"

// WSL Support
#ifndef __WSL__
#define ADPT_DEVICE "/sys/class/power_supply/ADP0/online"
#else
#define ADPT_DEVICE "/sys/class/power_supply/adapter/online"
#endif

#endif
